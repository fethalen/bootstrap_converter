bootstrap\_converter 0.4
Copyright 2016 Felix Thalén

bootstrap\_converter.py
====

### Introduction

bootstrap\_converter reads a Newick tree and then converts the bootstrap value
for each and every node in that tree into an integer value instead of a decimal
value. Some programs, such as TreSpEx, doesn't work well with non-integer
bootstrap values. This script is made to overcome exactly that problem.

bootstrap\_converter is released under the GNU General Public License 3.0. A copy of
this license is in the LICENSE file.

### Dependencies

bootstrap\_converter requires [Python](https://www.python.org/) to be installed
and it depends on the [ETE Toolkit](http://etetoolkit.org/). You can find
instructions for installing ETE at their [download
page](http://etetoolkit.org/download/).

### Usage

        usage: bootstrap_converter.py [-h] [-r] [-d] [-e] tree

        positional arguments:
          tree             file containing the tree

        optional arguments:
          -h, --help       show this help message and exit
          -r, --replace    replace the input file with the output
          -d, --decimal    integer -> decimal, default: decimal -> integer
          -e, --extension  specify extension, default: .mod.tre

### Example

Example `tree.tre` file:

        (A:1,(B:1,(E:1,D:1)0.43:0.5)0.73:0.5);

Example `tree.tre.mod.tre` output:

        (A:1,(B:1,(E:1,D:1)43:0.5)73:0.5);

If your tree file contains non-decimal values, the script will ignore them and
alert you, this makes it possible to keep files of different formats in the same
folder without breaking anything.

If you need to use this script for multiple trees, there is a shell script,
`batch_convert.sh` included in this repository. It should be straightforward
enough to modify according to your need.
