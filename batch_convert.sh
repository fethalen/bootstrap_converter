#!/bin/bash

set -e
set -u
set -o pipefail

for file in *.tre; do
    python bootstrap_converter.py "${file}"
done
